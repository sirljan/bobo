//
//  GameScene.swift
//  BoBo
//
//  Created by Jenda Širl on 03/12/2016.
//  Copyright © 2016 Jenda Širl. All rights reserved.
//

import SpriteKit
import GameplayKit

private let kMovableNode = "movable"
private var kLogEnabled = false


extension UIGestureRecognizer {
    func stateString() -> String {
        switch self.state {
        case .possible:
            return "possible"
        case .began:
            return "began"
        case .changed:
            return "changed"
        case .ended:
            return "ended"
        case .cancelled:
            return "cancled"
        case .failed:
            return "failed"
        }
    }
}

func JSLog(_ string:String) {
    if kLogEnabled {
        NSLog(string)
    }
}


class GameScene: SKScene, UIGestureRecognizerDelegate {
    var tapGestureRecognizer:UITapGestureRecognizer!
    var panGestureRecognizer:UIPanGestureRecognizer!
    
    var red:SKSpriteNode?
    var green:SKSpriteNode?
    
    var selectedNode:SKNode?
    
    override func didMove(to view: SKView) {
        self.tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GameScene.handleTap(tapGestureRecognizer:)))
        self.panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(GameScene.handlePan(panGestureRecognizer:)))
        self.view!.addGestureRecognizer(tapGestureRecognizer)
        self.view!.addGestureRecognizer(panGestureRecognizer)
        
        if let red = self.childNode(withName: "*\(kMovableNode).red*") as? SKSpriteNode {
            self.red = red
        }
        if let green = self.childNode(withName: "*\(kMovableNode).green*") as? SKSpriteNode {
            self.green = green
        }

    }
    
    // isDynimac is set here because it is recognized faster then gesture
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        NSLog("\(#function)")
        kLogEnabled = true
        if let touch = touches.first {
            let touchLocationScene = touch.location(in: self)
            let canditateNode = self.touchedNode(touchLocationScene)
            if let name = canditateNode.name, name.contains(kMovableNode) {
                self.selectedNode = canditateNode
                self.selectedNode?.physicsBody?.isDynamic = false;
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    if  self.panGestureRecognizer.state != .began ||
        self.panGestureRecognizer.state != .changed ||
        self.panGestureRecognizer.state != .ended {
            NSLog("\(#function)")
        }
    }
    
    func handlePan(panGestureRecognizer recognizer:UIPanGestureRecognizer) {
        let touchLocationView = recognizer.location(in: recognizer.view)
        let touchLocationScene = self.convertPoint(fromView: touchLocationView)
        
        NSLog("state %@", recognizer.stateString())
        
        switch recognizer.state {
        case .began:
            let canditateNode = self.touchedNode(touchLocationScene)
            if let name = canditateNode.name, name.contains(kMovableNode) {
                self.selectedNode = canditateNode
            }
        case .changed:
            let translation = recognizer.translation(in: recognizer.view)
            if let position = self.selectedNode?.position {
                self.selectedNode?.position = CGPoint(x: position.x + translation.x, y: position.y - translation.y)
                recognizer.setTranslation(CGPoint.zero, in: recognizer.view)
            }
        case .ended:
            self.selectedNode?.physicsBody?.isDynamic = true;
            let velocity = recognizer.velocity(in: recognizer.view)
            self.selectedNode?.physicsBody?.applyImpulse(CGVector(dx: velocity.x, dy: -velocity.y))
            self.selectedNode = nil
            kLogEnabled = false
        default:
            break
        }
    }
    
    func handleTap(tapGestureRecognizer recognizer:UITapGestureRecognizer)
    {
        if recognizer.state == .ended {
            let touchLocationView = recognizer.location(in: recognizer.view)
            let touchLocationScene = self.convertPoint(fromView: touchLocationView)
            _ = self.touchedNode(touchLocationScene)
        }
    }

    func touchedNode(_ touchLocationInScene:CGPoint) -> SKNode {
        let node = self.atPoint(touchLocationInScene)
        print("touchedNode: \(node.name)")
        return node
    }
    
    override func update(_ currentTime: TimeInterval) {
        JSLog("\(#function)")
    }
    override func didEvaluateActions() {
        JSLog("\(#function)")
    }
    override func didSimulatePhysics() {
        JSLog("\(#function)")
    }
    override func didApplyConstraints() {
        JSLog("\(#function)")
    }
    override func didFinishUpdate() {
        JSLog("\(#function)")
    }

}
